const { Sequelize, DataTypes, Model } = require("sequelize");

//configuramos nuestra instancia de Sequelize para

const sequelize = new Sequelize("acamicadb", "root", "", {
  host: "localhost",
  dialect: "mysql",
});

//verificamos que se conecte a la base de datos mysqld

sequelize
  .authenticate()
  .then(() => {
    console.log("Conexion satisfactoria");
  })
  .catch(() => {
    console.log("Conexion con problemas");
  });

class Marcas extends Model {}

Marcas.init(
  {
    nombre: DataTypes.STRING,    
  },
  {
    sequelize,
    modelName: "marcas",
    timestamps:false,//para remover las columnas de createdAt,updatedAt
  }
);

class Modelos extends Model {}
Modelos.init(
    {
      nombre: DataTypes.STRING, //nombre de la casa
    },
    { 
        sequelize, 
        modelName: "modelos", 
        timestamps:false,//para remover las columnas de createdAt,updatedAt 
    }

  );


  //asociacion usando belongsTo
  //si la llave foranea la va a tener la primera tabla usamos belongsTo
  //Usuarios.belongsTo(Casas,{foreignKey:'id_casa'});
  //si la llave foranea la va a tener la segunda tabla se usa hasOne
  Marcas.hasOne(Modelos,{foreignKey:'marca_id'});

//funcion autoinvocada para realizar las operaciones

(async () => {

    
  //insertar datos
  await sequelize.sync({force:true});



})();
